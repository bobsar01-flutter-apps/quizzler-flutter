import 'package:rflutter_alert/rflutter_alert.dart';

class Alerts {
  void _basicAlert(context, score) {
    Alert(
      context: context,
      title: "Finished!",
      desc: "You've reached the end of the quiz.\n\nScore: $score",
    ).show();
  }

  void getAlert(context, score) {
    _basicAlert(context, score);
  }
}
// Alert with single button.
//  _alertButton(context) {
//    Alert(
//      context: context,
//      type: AlertType.error,
//      title: "RFLUTTER ALERT",
//      desc: "Flutter is more awesome with RFlutter Alert.",
//      buttons: [
//        DialogButton(
//          child: Text(
//            "COOL",
//            style: TextStyle(color: Colors.white, fontSize: 20),
//          ),
//          onPressed: () => Navigator.pop(context),
//          width: 120,
//        )
//      ],
//    ).show();
//  }

// Alert with multiple and custom buttons
//  _alertButtons(context) {
//    Alert(
//      context: context,
//      type: AlertType.warning,
//      title: "RFLUTTER ALERT",
//      desc: "Flutter is more awesome with RFlutter Alert.",
//      buttons: [
//        DialogButton(
//          child: Text(
//            "FLAT",
//            style: TextStyle(color: Colors.white, fontSize: 20),
//          ),
//          onPressed: () => Navigator.pop(context),
//          color: Color.fromRGBO(0, 179, 134, 1.0),
//        ),
//        DialogButton(
//          child: Text(
//            "GRADIENT",
//            style: TextStyle(color: Colors.white, fontSize: 20),
//          ),
//          onPressed: () => Navigator.pop(context),
//          gradient: LinearGradient(colors: [
//            Color.fromRGBO(116, 116, 191, 1.0),
//            Color.fromRGBO(52, 138, 199, 1.0)
//          ]),
//        )
//      ],
//    ).show();
//  }

// Advanced using of alerts
//  _alertWithStyle(context) {
//    // Reusable alert style
//    var alertStyle = AlertStyle(
//        animationType: AnimationType.fromTop,
//        isCloseButton: false,
//        isOverlayTapDismiss: false,
//        descStyle: TextStyle(fontWeight: FontWeight.bold),
//        animationDuration: Duration(milliseconds: 400),
//        alertBorder: RoundedRectangleBorder(
//          borderRadius: BorderRadius.circular(0.0),
//          side: BorderSide(
//            color: Colors.grey,
//          ),
//        ),
//        titleStyle: TextStyle(
//          color: Colors.red,
//        ),
//        constraints: BoxConstraints.expand(width: 300));
//
//    // Alert dialog using custom alert style
//    Alert(
//      context: context,
//      style: alertStyle,
//      type: AlertType.info,
//      title: "RFLUTTER ALERT",
//      desc: "Flutter is more awesome with RFlutter Alert.",
//      buttons: [
//        DialogButton(
//          child: Text(
//            "COOL",
//            style: TextStyle(color: Colors.white, fontSize: 20),
//          ),
//          onPressed: () => Navigator.pop(context),
//          color: Color.fromRGBO(0, 179, 134, 1.0),
//          radius: BorderRadius.circular(0.0),
//        ),
//      ],
//    ).show();
//  }

//
// Alert custom images
//  _alertWithCustomImage(context) {
//    Alert(
//      context: context,
//      title: "RFLUTTER ALERT",
//      desc: "Flutter is more awesome with RFlutter Alert.",
//      image: Image.asset("assets/success.jpg"),
//    ).show();
//  }

//// Alert custom content
//  _alertWithCustomContent(context) {
//    Alert(
//        context: context,
//        title: "LOGIN",
//        content: Column(
//          children: <Widget>[
//            TextField(
//              decoration: InputDecoration(
//                icon: Icon(Icons.account_circle),
//                labelText: 'Username',
//              ),
//            ),
//            TextField(
//              obscureText: true,
//              decoration: InputDecoration(
//                icon: Icon(Icons.lock),
//                labelText: 'Password',
//              ),
//            ),
//          ],
//        ),
//        buttons: [
//          DialogButton(
//            onPressed: () => Navigator.pop(context),
//            child: Text(
//              "LOGIN",
//              style: TextStyle(color: Colors.white, fontSize: 20),
//            ),
//          )
//        ]).show();
//  }
