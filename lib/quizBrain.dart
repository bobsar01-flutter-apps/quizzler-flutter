import 'package:quizzler/alert.dart';
import 'package:flutter/material.dart';
import 'question.dart';

class QuizBrain {
  int _questionNo = 0;
  int _correctTotal = 0;
  List<Widget> scoreKeeper = [];
  List<bool> scoreKeeperBool = [];

  Alerts alert = new Alerts();

  void _resetQuiz() {
    _questionNo = 0;
    _correctTotal = 0;
    scoreKeeper = [];
    scoreKeeperBool = [];
  }

  void nextQuestion(BuildContext context) {
    if (_questionNo < Question.getQuestionBank().length - 1) {
      _questionNo++;
    } else {
      for (int i = 0; i < scoreKeeper.length; i++) {
        if (scoreKeeperBool[i]) {
          _correctTotal++;
        }
      }
      String score = "$_correctTotal/${scoreKeeper.length}";
      alert.getAlert(context, score);

      _resetQuiz();
    }
  }

  String getQuestionText() {
    return Question.getQuestionBank()[_questionNo].questionText;
  }

  bool getQuestionAns() {
    return Question.getQuestionBank()[_questionNo].questionAns;
  }
}
